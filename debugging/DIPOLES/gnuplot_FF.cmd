          dec = 0.01  # just to distinguish various curves on the graph


plot \
'fort.88' u ($6==1 ? $7 : 1/0):9 w lp ps .2 pt 5 lw 2 lt 1 lc rgb "red" tit "bz_1" ,\
'fort.88' u ($6==2 ? $7 : 1/0):9 w lp ps .2 pt 5 lw 2 lt 1 lc rgb "blue" tit "bz_2" ,\
'fort.88' u ($6==2 ? $7 : 1/0):10 w lp ps .2 pt 5 lw 2 lt 1 lc rgb "black"  tit "bz_{1+2" 

 set samples 10000
 set terminal postscript eps blacktext color enh size 8cm,5cm 
 set output "gnuplot_FF.eps"
 replot
 set terminal X11
 unset output

pause 1
exit
