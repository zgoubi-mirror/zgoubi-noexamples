C  ZGOUBI, a program for computing the trajectories of charged particles
C  in electric and magnetic fields
C  Copyright (C) 1988-2007  François Méot
C
C  This program is free software; you can redistribute it and/or modify
C  it under the terms of the GNU General Public License as published by
C  the Free Software Foundation; either version 2 of the License, or
C  (at your option) any later version.
C
C  This program is distributed in the hope that it will be useful,
C  but WITHOUT ANY WARRANTY; without even the implied warranty of
C  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C  GNU General Public License for more details.
C
C  You should have received a copy of the GNU General Public License
C  along with this program; if not, write to the Free Software
C  Foundation, Inc., 51 Franklin Street, Fifth Floor,
C  Boston, MA  02110-1301  USA
C
C  François Méot <fmeot@bnl.gov>
C  Brookhaven National Laboratory
C  C-AD, Bldg 911
C  Upton, NY, 11973, USA
C  -------
      SUBROUTINE SVDOC(KLE,LABEL,
     >                           READAT)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      CHARACTER(*) KLE(*)
      INCLUDE 'MXLD.H'
      CHARACTER(*) LABEL(MXL,2)
      LOGICAL READAT
      INCLUDE "C.CDF.H"     ! COMMON/CDF/ IES,LF,LST,NDAT,NRES,NPLT,NFAI,NMAP,NSPN,NLOG
      PARAMETER (MXPUD=9,MXPU=1000)
      INCLUDE "C.CO.H"     ! COMMON/CO/ FPU(MXPUD,MXPU),KCO,NPUF,NFPU,IPU
      PARAMETER (MCOF=5)
      PARAMETER (LBLSIZ=20)
      CHARACTER(LBLSIZ) COLAB
      INCLUDE 'C.COC.H'     ! COMMON/COC/ COLAB(MCOF)
      INCLUDE "C.CONST2.H"     ! COMMON/CONST2/ ZERO, UN
      PARAMETER (MPUF=6)
      CHARACTER(LBLSIZ) PULAB
      INCLUDE 'C.COT.H'     ! COMMON/COT/ PULAB(MPUF)
      INCLUDE "C.DON.H"     ! COMMON/DON/ A(MXL,MXD),IQ(MXL),IP(MXL),NB,NOEL
      INCLUDE "C.DONT.H"     ! COMMON/DONT/ TA(MXL,MXTA)
      INCLUDE "MAXCOO.H"
      INCLUDE "C.REBELO.H"   ! COMMON/REBELO/ NRBLT,IPASS,KWRT,NNDES,STDVM

      LOGICAL OKCOR
      SAVE NBLM, OKCOR
      PARAMETER (T2KG = 10.D0)

      PARAMETER (IMON=MPUF/3)
      PARAMETER(MXPUH =IMON, MXPUV =IMON)
      CHARACTER(LBLSIZ) HPNA(MXPUH), VPNA(MXPUV), HVPNA(MXPUV)
      CHARACTER(LBLSIZ) HPNAI(MXPUH), VPNAI(MXPUV), HVPNAI(MXPUV)
      SAVE HPNA, VPNA, HVPNA

      PARAMETER (MCHF=5, MCVF=5)
      CHARACTER(LBLSIZ) HCNA(MCHF), VCNA(MCVF)
      CHARACTER(LBLSIZ) HCNAI(MCHF), VCNAI(MCVF)
      SAVE HCNA, VCNA

      SAVE NLMC, NLM, KSCOR

      LOGICAL IDLUNI
      CHARACTER(70) TXFMT
      SAVE TXFMT
      SAVE LSVD

      LOGICAL FITRBL
      INTEGER(4) TODAY(3)

      SAVE NOELB

      PARAMETER (MXCO=1000)
      CHARACTER(132) NAMPU(MXPU), NAMCO(MXCO)
      DIMENSION KHV(MXPU)       ! 1, 2, 3 FOR H V, HV

      SAVE NLM1, ANLM1
      LOGICAL FITFNL

      DIMENSION TMP(MXPU)
      CHARACTER(80) NAMFIL

      CHARACTER(1) TXT
      PARAMETER (CM2M = 1.D-2)

      DIMENSION AA(MXPU,MXCO)

      DIMENSION IQLCO(MXCO)


      DATA FITRBL / .FALSE. /
      DATA NOELA, NOELB / 1, MXL /
      DATA ITER / 0 /
      DATA NAMFIL / 'zgoubi.SVD.out' /

C Build SVD matrix:
C Scan over correctors. For each: 1/ change it 2/ find orbit 3/ log PUs

      INF = NINT(A(NOEL,1))
      KWRT = NINT(A(NOEL,2))
      NAMFIL = TA(NOEL,1)(1:80)
      HKIC = A(NOEL,10)
      VKIC = A(NOEL,11)
      JNF = NINT(A(NOEL,20))
      ITER = NINT(A(NOEL,22))

      CALL REBLT5(
     >            NOELA, NOELB)     !  1 and NOEL(SVDOC)

      IF(IPASS.EQ.1) THEN

C        KSCOR = 1              ! H corr first
        NLMC = 0
        NLM = 0
        CALL ZGNBLM(
     >              NBLMI)
        NBLM = NBLMI

C Fill PULAB with PU names
        CALL SVDPUS(NBLM,HPNA,VPNA,HVPNA,
     >                        NAMPU,NPUF,NPUH,NPUV,NPUHV,KHV)
        NPUS = NPUH+NPUV+NPUHV
        NPU = NPUF
        KCO = 2
        CALL SVDPC0            ! Set PU counter to zero

Check corrector families
        CALL SVDCOS(NBLM,HCNA,VCNA,IQLCO,
     >                        NAMCO,NCOF,NCOH,NCOV)
        NCOS = NCOH+NCOV
        IF    (NCOH.GE.1) THEN
          KSCOR = 1      ! H kick
        ELSEIF(NCOV.GE.1) THEN
          KSCOR = 2      ! V kick
        ELSE
          CALL ENDJOB('Pgm svdoc. No correctors declared. Leaving',-99)
        ENDIF
        WRITE(NRES,
     >  FMT='(5X,''SVD correction matrix requested:'',/,
     >  10X,''A total of '',I0,'' PUs ('',I0,
     >  '' H and '',I0,'' V),  in '',I0,'' families : ''
     >       ,5(A,1X))')
     >  NPUS,NPUH,NPUV,NPUF,(TRIM(PULAB(I)),I=1,NPUF)
        WRITE(NRES,FMT='(10X,''and of '',I0,'' corrctrs ('',I0,
     >  '' H and '',I0,'' V),  in '',I0,'' families : ''
     >  ,5(A,1X),/)')
     >  NCOS,NCOH,NCOV,NCOF,(TRIM(COLAB(I)),I=1,NCOF)
        WRITE(NRES,FMT='(10X,''Corrector kick values for SVD matrix''
     >  ,'' computation: '',1P,5(1X,E12.4))') HKIC, VKIC
C     >  ,1P,5(1X,E12.4))') A(NOEL,10),A(NOEL,11)
        WRITE(NRES,*) ' '

        CALL ERROR1(
     >              IONF)
        IF(IONF .NE. 0) CALL ENDJOB('Pgm svdoc. ERRORS switch must'
     >  //' be set to OFF (ONF=0), its management lies with SVDOC.'
     >  ,-99)

        IF(IDLUNI(
     >            LSTPC)) THEN
C          OPEN(UNIT=LSTPC,FILE=NAMFIL)
          OPEN(UNIT=LSTPC,FILE='zgoubi.SVD_PuCoList.out')
          CALL IDATE(TODAY)
          WRITE(LSTPC,FMT='(''# '',2(I2.2,A1),I4.4,
     >    ''. zgoubi.SVD_PuCoList.out, from svdoc.f:'',
     >    '' pickup and corrector lists. '')')
     >    TODAY(1),'-',TODAY(2),'-',TODAY(3)
          WRITE(LSTPC,FMT='(A,3X,I0)') '# PU list :       ',NPUS
          WRITE(LSTPC,FMT='(T5,''#'',T10,''KEY'',T21,''LBL1'',T40,
     >    ''LBL2'',T60,''NOEL'',T70,''s /cm'')')
          DO I = 1, NPUS
            READ(NAMPU(I),*)  TXT,TXT,TXT,NLPU
            CALL SCUM5(NLPU,
     >                      SCML,TCML)
c            write(*,*) ' svdoc ',i,nlpu,scml
c            read(*,*)
            WRITE(LSTPC,FMT='(I6,3X,A,1P,2X,E14.6)')
     >      I,TRIM(NAMPU(I)),SCML*CM2M
          ENDDO
          WRITE(LSTPC,FMT='(A,3X,I0)') '# Corrector list : ',NCOS
          WRITE(LSTPC,FMT='(T5,''#'',T10,''KEY'',T21,''LBL1'',T40,
     >    ''LBL2'',T60,''NOEL'',T70,''s /cm'')')
          DO I = 1, NCOS
            READ(NAMCO(I),*)  TXT,TXT,TXT,NLCO
            CALL SCUM5(NLCO,
     >                      SCML,TCML)
            WRITE(LSTPC,FMT='(I6,3X,A,1P,2X,E14.6)')
     >      I,TRIM(NAMCO(I)),SCML*CM2M
          ENDDO
          CLOSE(LSTPC)
        ELSE
          CALL ENDJOB('Pgm svdoc. Could not open file '
     >    //'zgoubi.SVD_PuCoList.out',-99)
        ENDIF

C        IF(NPUS .LT. NCOS) CALL ENDJOB('Pgm svdoc. Numb of PUs must be'
C     >  //' .ge. numb of correctors !',-99)

        IF(IDLUNI(
     >            LSVD)) THEN

          OPEN(UNIT=LSVD,FILE='zgoubi.SVDtmp.out')
          CALL IDATE(TODAY)
          WRITE(LSVD,FMT='(''# '',2(I2.2,A1),I4.4,
     >    ''. zgoubi.SVDtmp.out, from svdoc.f.'')')
     >    TODAY(1),'-',TODAY(2),'-',TODAY(3)
          WRITE(LSVD,FMT='(''# Columns: PU records 1 to '',I0
     >    ,'' (=#PUH+#PUV+2*#PUHV)'',
     >    '', ordering follows zgoubi.res optical sequence.'')')
     >    NPUS  !+NPUHV
          WRITE(LSVD,FMT='(''# Rows: corrector 1 to '',I0,
     >    '', ordering follows zgoubi.res optical sequence.'')')
     >         NCOS

          WRITE(LSVD,FMT='(
     >    ''# A total of '',I0,'' PUs ('',I0,''H, '',I0,''V, '',
     >    I0,''HV),'','' in '',I0,'' PU families : '',5(A,1x))') NPUS,
     >    NPUH,NPUV,NPUHV,NPUF,(TRIM(PULAB(I)),I=1,NPUF)
          WRITE(LSVD,FMT='(''# and of '',I0,'' corrctrs ('',I0,
     >    '' H and '',I0,'' V),  in '',I0,'' families : ''
     >    ,5(A,1X))')
     >    NCOS,NCOH,NCOV,NCOF,(TRIM(COLAB(I)),I=1,NCOF)
          WRITE(LSVD,FMT='(''# '',/,''# '')')

          WRITE(TXFMT,FMT='(A,I0,A,A)') '(1P,',
     >    NPUS,'(E12.4,1X),','3(1X,I0),1X,A,1X,E14.6)'
C     >    NPUS+NPUHV,'(E12.4,1X),','3(1X,I0),1X,A,1X,E14.6)'

          WRITE(LSVD,FMT='(''# svdoc|svdpr. Transpose A = '')')

        ELSE

          CALL ENDJOB('Pgm svdoc. Could not open file '
     >    //'zgoubi.SVDtmp.out.',-99)
        ENDIF

      ELSE     ! IPASS.GT.1

        IF(NLMC.GE.1 .AND. NLM .GE. 1)
     >        CALL SVDPR(LSVD,TXFMT,NLM,NLMC,    !!! NPUS,NCOS,
     >                   KLE,IPASS,LABEL,HKIC,VKIC,
     >                                                  AA)

      ENDIF

      FLUSH(LSVD)

C Loop over corrector excitation, one-by-one. FIT finds the orbit each time.
C      IF(KSCOR.LE.2) THEN
C There are ! 2 families of correctors at the moment

      IF(NLM .GT. 1 .AND. NLM1 .LT. NBLM) THEN
        A(NLM1,4) = ANLM1
c             write(88,*) 'kscor anlm1 : ',kscor,nlm,anlm1
      ENDIF

      OKCOR = .FALSE.
      DO WHILE ((.NOT. OKCOR) .AND. NLM .LT. NBLM)
C Move to next corrector. NLMC (1<NLMC<NBLM) is its number in the A() list
        NLM = NLM + 1
C        OKCOR =
C     >  (KSCOR .EQ. 1 .AND. LABEL(NLM,1).EQ.'HKIC')
C     >  .OR.
C     >  (KSCOR .EQ. 2 .AND. LABEL(NLM,1).EQ.'VKIC')

        OKCOR = IQLCO(NLMC+1) .EQ. NLM
      ENDDO

      IF(NLM .GE. NBLM) THEN
        A(NLM1,4) = ANLM1
        IF(IPASS .LE. NCOR+2) WRITE(ABS(NRES),FMT='(5X,''IPASS = '',I6,
     >  ''.  Previous element, noel = '',I6
     >  ,'' reset to A('',i0,'',4) = '',e12.4
     >  )') IPASS,NLM1,NLM1,A(NLM1,4)
        OKCOR=.FALSE.
C        KSCOR = KSCOR + 1
        NLM = 0
      ENDIF

      IF(OKCOR) THEN
        NLMC = NLMC + 1
        OKCOR=.FALSE.
        IF(NLM .LE. NBLM) THEN
          NLM2 = NLM1
          NLM1 = NLM
          ANLM1 = A(NLM,4)
          IF(LABEL(NLM,1).EQ.'HKIC') THEN   !              IF    (KSCOR .EQ. 1) THEN
            A(NLM,4) = HKIC / (A(NLM,2)*1.D-2) * T2KG    ! B = (Brho==1)*kick/L
C            A(NLM,4) = A(NOEL,10) / (A(NLM,2)*1.D-2) * T2KG    ! B = (Brho==1)*kick/L
C           write(88,*) nlm, a(nlm,4) ,' cor1 noel, a(noel,4)   svdoc '
          ELSEIF(LABEL(NLM,1).EQ.'VKIC') THEN         ! KSCOR .EQ. 2) THEN
            A(NLM,4) = VKIC / (A(NLM,2)*1.D-2) * T2KG    ! B = (Brho==1)*kick/L
             A(NLM,4) =  A(NOEL,11) / (A(NLM,2)*1.D-2) * T2KG    ! B = (Brho==1)*kick/L
C           write(88,*) nlm, a(nlm,4) ,' cor2 noel, a(noel,4)   svdoc '
          ELSE
            CALL ENDJOB(
     >      'Pgm svdoc. No such family option : '//TRIM(LABEL(NLM,1))
     >      ,-99)
          ENDIF

          WRITE(ABS(NRES),FMT='(5X,''IPASS = '',I6,
     >    ''.  Changed element NOEL= '',I6,'' to A('',I0,'',4) = ''
     >    ,1P,E12.4)') IPASS, NLM,NLM,A(NLM,4)
          IF(IPASS .GT. 1) WRITE(ABS(NRES),
     >    FMT='(5X,''Previous element, noel = '',I6
     >    ,'' reset to A('',I0,'',4) = '',E12.4
     >    )') NLM2,NLM2,A(NLM2,4)

        ENDIF
      ENDIF

      WRITE(ABS(NRES),100) IPASS
 100  FORMAT(/,30X,'SVDOC. End of pass # ',
     >I0,' through the optical structure ',/)

C      NRBLT = NCOS+2 +3
      NRBLT = NCOS+2 +2 + ITER*JNF
      IPASS=IPASS+1

C      IF( IPASS .LE. NRBLT+1 -2) THEN
      IF( IPASS .LE. NRBLT -1) THEN

        READAT = .FALSE.
        NOEL=NOELA-1

C        IF(IPASS .LT. NRBLT+1 -2) THEN
        IF(IPASS .LT. NRBLT -1) THEN

          WRITE(ABS(NRES),FMT='(2(10X,A,/))')
     >    'Computation of SVD matrix on-going: loop on single-corrector'
     >    //' excitation & PUs reading;'
     >    //' vectors logged to zgoubi.SVDtmp.out.'

C        ELSEIF(IPASS .EQ. NRBLT+1 -2) THEN
        ELSEIF(IPASS .EQ. NRBLT -1) THEN

C Inverse the matrix
          CALL SVDINV(LSVD,NPUS,NCOS,NAMFIL,AA)

c           write(*,*) ' svdoc 4'
c           CALL SVDINV(LSVD,NPUS,NCOS,NAMFIL)
C           CALL SVDPR3(NAMFIL)
c           write(*,*) ' svdoc 5'


C          CALL FLUSH(LSVD)
          WRITE(ABS(NRES),FMT='(2(10X,A,/))')
     >    'The A matrix is now built. Logged to zgoubi.SVDtmp.out.',
     >    'SVD-inverted matrix A^-1 logged to '//TRIM(NAMFIL)//'.'       ! zgoubi.SVD.out
          WRITE(ABS(NRES),FMT='(10X,A,/,2(15X,A,/))')
     >    'Plan now :  ',
     >    '1- activate ERRORS, this will inject defects,' ,
     >    '2- next pass will FIT the resulting orbit (will log '
     >    //'it to zgoubi.SVDOrbits.out).'

          WRITE(ABS(NRES),FMT='(10X,A,/)')
     >    'Now switch on ERRORS. '//
     >    'Next pass will find defect orbit (saved, for the record).'
C     >    'Now applying errors (from ERRORS), together orbit correction'
C     >    //' using A^-1.'
          CALL ERROR3(
     >                NUML)
          A(NUML,1) = 1           ! Set ERROR switch to ON.

C Final pass by FIT will get the closed  orbit's PU vector
          FITFNL = .FALSE.        ! This is necessary to allow setting errors in multpo...
          CALL FITST6(FITFNL)     ! anyway it will be set back to whatever needed by FIT in due time.

        ENDIF

      ELSEIF(IPASS .EQ. NRBLT) THEN
C Given PU vector from latest orbit,
C apply SVD matrix to update corrector fields -> corr = -A^-1 * PUs
        CALL SVDCMD(NAMFIL,IPASS,NAMPU,NAMCO,NCOS,KHV,NPUS,IQLCO,AA,
     >                                                        LW)
        WRITE(ABS(NRES),FMT='(2(10X,A,/))')
     >  'Just applied SVD matrix to correctors: Corr = -A^-1 * PU.'
     >  ,'Next pass is last pass: will find (FIT) the '
     >  //'orbit, corrected (hopefully) from ERRORS defects.'

C FIT new orbit
        READAT = .FALSE.
        NOEL=NOELA-1

C      ELSEIF(IPASS .EQ. NRBLT+1) THEN
      ELSEIF(IPASS .GE. NRBLT+1) THEN

        JPU = 0
        DO I = 1, NPUS
          IF    (KHV(I) .EQ. 1) THEN
            JPU = JPU + 1
            TMP(JPU) = FPU(2,I)
          ELSEIF(KHV(I) .EQ. 2) THEN
            JPU = JPU + 1
            TMP(JPU) = FPU(4,I)
          ELSE
            JPU = JPU + 2
            TMP(JPU-1) = FPU(2,I)
            TMP(JPU) = FPU(4,I)
          ENDIF
        ENDDO

        CALL FITWDA(
     >              IER)

        WRITE(LW,*) '# PU#    PU corrected   H/V  IPASS    #PU   #CO  '
     >  //'NOEL(PU)    s(PU)/m         from svdoc'
        DO I = 1, NPUS
          READ(NAMPU(I),*)  TXT,TXT,TXT,NLPU
          CALL SCUM5(NLPU,
     >                    SCML,TCML)
c          write(*,*) ' svdoc ',i,nlpu,scml
c          read(*,*)
          WRITE(LW,FMT='(I6,1X,1P,E14.6,1X,5(I6,1X),E14.6)')
     >    I,TMP(I),KHV(I),IPASS,NPUS,NCOS,NLPU,SCML*CM2M
        ENDDO

      ENDIF

C     CLOSE(LSVD)
      CALL SVDPC0      ! Reset PU counter
      CALL SCUMS(ZERO) ! Reset cumulated length of sequence

      RETURN

      ENTRY SVDOC2(HPNAI,VPNAI,HVPNAI,HCNAI,VCNAI)
!  HPU & HCorr name list, VPU & VCorr name list
      HPNA =       HPNAI
      VPNA =       VPNAI
      HVPNA =       HVPNAI
      HCNA =       HCNAI
      VCNA =       VCNAI
      RETURN

      END
