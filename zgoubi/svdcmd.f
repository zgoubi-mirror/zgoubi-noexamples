C  ZGOUBI, a program for computing the trajectories of charged particles
C  in electric and magnetic fields
C  Copyright (C) 1988-2007  François Méot
C
C  This program is free software; you can redistribute it and/or modify
C  it under the terms of the GNU General Public License as published by
C  the Free Software Foundation; either version 2 of the License, or
C  (at your option) any later version.
C
C  This program is distributed in the hope that it will be useful,
C  but WITHOUT ANY WARRANTY; without even the implied warranty of
C  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C  GNU General Public License for more details.
C
C  You should have received a copy of the GNU General Public License
C  along with this program; if not, write to the Free Software
C  Foundation, Inc., 51 Franklin Street, Fifth Floor,
C  Boston, MA  02110-1301  USA
C
C  François Méot <fmeot@bnl.gov>
C  Brookhaven National Laboratory
C  C-AD, Bldg 911
C  Upton, NY, 11973, USA
C  -------
      SUBROUTINE SVDCMD(NAMFIL,IPASS,NAMPU,MCO,KHV,MPU,IQLCO,AA,
     >                                                       LW)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
C     -----------------------------
C     Apply A^-1 *PUs to correctors
C     -----------------------------
C      DIMENSION AA(MPU,MCO)
      DIMENSION AA(MCO,MPU)   ! A^-1
      CHARACTER(*) NAMFIL
      CHARACTER(*) NAMPU(*)
      DIMENSION IQLCO(*)
      INCLUDE 'C.CDF.H'         ! COMMON/CDF/ IES,LF,LST,NDAT,NRES,NPLT,NFAI,NMAP,NSPN,NLOG
      PARAMETER (MXPUD=9,MXPU=1000)
      INCLUDE 'C.CO.H'     ! COMMON/CO/ FPU(MXPUD,MXPU),KCO,NPUF,NFPU,IPU
      INCLUDE 'MXLD.H'
      INCLUDE 'C.DON.H'     ! COMMON/DON/ A(MXL,MXD),IQ(MXL),IP(MXL),NB,NOEL
      PARAMETER (LBLSIZ=20)
      CHARACTER(LBLSIZ) LABEL
      INCLUDE 'C.LABEL.H'     ! COMMON/LABEL/ LABEL(MXL,2)

      DIMENSION KHV(MXPU)   ! 1, 2, 3 FOR H V, HV
      DIMENSION TMP(MPU)

      DIMENSION AM1(MCO,MPU), COR(MCO)
      LOGICAL IDLUNI, OPN
      SAVE OPN
      CHARACTER(1) TXT

      LOGICAL OKCO
      PARAMETER (T2KG = 10.D0)
      PARAMETER (CM2M = 1.D-2)

      LOGICAL OKRD

      DATA OPN / .FALSE. /
      DATA OKRD / .TRUE. /

      JPU = 0
      DO I = 1, MPU
         IF    (KHV(I) .EQ. 1) THEN
          JPU = JPU + 1
          TMP(JPU) = FPU(2,I)
        ELSEIF(KHV(I) .EQ. 2) THEN
          JPU = JPU + 1
          TMP(JPU) = FPU(4,I)
       ELSE
          JPU = JPU + 2
          TMP(JPU-1) = FPU(2,I)
          TMP(JPU) = FPU(4,I)
C            write(*,*) ' khv = else'
        ENDIF
      ENDDO

      IF(OKRD) THEN
        IF(IDLUNI(
     >            LR)) OPEN(UNIT=LR,FILE=NAMFIL,STATUS='OLD')  ! default is zgoubi.SVD.out
C Read 2-line header
        READ(LR,*) TXT
        READ(LR,*) TXT

        DO ILN = 1, MCO
          READ(LR,*,ERR=88,END=88) (AM1(ILN,JCL), JCL = 1, MPU)
        ENDDO
        CLOSE(LR)
      ELSE
         AM1(1:MCO,1:MPU) = AA(1:MCO,1:MPU)
      ENDIF

      COR = MATMUL(AM1,TMP)

c        write(*,*) 'svdcmd.  tmp(ipu) : ',mpu, (tmp(jj),jj=1,mpu)
c        write(*,*) 'svdcmd.  a^-1 : '
c        do ii = 1, mco
c        write(*,*) (am1(ii,jj),ii,jj,jj=1,mpu)
c      enddo
c      write(*,*) ' svdcmd.  cor : ',mco, (cor(ii),ii=1,mco)
c           read(*,*)

      IF(.NOT. OPN) THEN
        OPN = IDLUNI(
     >                LW)
        OPEN(UNIT=LW,FILE='zgoubi.SVDOrbits.out')
      ENDIF
      WRITE(LW,*) '# PU#  PU uncorrected   H/V  IPASS    #PU   #CO  '
     >//'NOEL(PU)    s(PU)/m         from svdcmd'
      DO I = 1, MPU
        READ(NAMPU(I),*)  TXT,TXT,TXT,NLPU
        CALL SCUM5(NLPU,
     >                  SCML,TCML)
        WRITE(LW,FMT='(I6,1X,1P,E14.6,1X,5(I6,1X),E14.6)')
     >       I,TMP(I),KHV(I),IPASS,MPU,MCO,NLPU,SCML*CM2M
      ENDDO
      WRITE(LW,*) ' '

      CALL ZGNBLM(
     >            NBLMI)
      NBLM = NBLMI
      NLM = 1
      OKCO = .FALSE.
      DO WHILE (IC.LT.MCO .AND. NLM .LE. NBLM)
C Move to next element in sequence. Test whether its label identifies w/ C.O. label.

        IC = 1
        DO WHILE((.NOT. OKCO) .AND. IC.LE.MCO .AND. NLM.LT.NBLM)
          OKCO = NLM .EQ. IQLCO(IC)
          IF(OKCO) THEN
C I do not understand the factor *1e-3:
            A(NLM,4)= -COR(IC) /(A(NLM,2)*1.D-2) *T2KG  ! B=(Brho==1)*kick/L
            IC = IC + 1
          ENDIF
          NLM = NLM + 1
          OKCO = .FALSE.
        ENDDO
      ENDDO

      RETURN

 88   CALL ENDJOB('Pgm svdcmd. Error @ read logical unit LR=',LR)
      RETURN
      END
