C  ZGOUBI, a program for computing the trajectories of charged particles
C  in electric and magnetic fields
C  Copyright (C) 1988-2007  François Méot
C
C  This program is free software; you can redistribute it and/or modify
C  it under the terms of the GNU General Public License as published by
C  the Free Software Foundation; either version 2 of the License, or
C  (at your option) any later version.
C
C  This program is distributed in the hope that it will be useful,
C  but WITHOUT ANY WARRANTY; without even the implied warranty of
C  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C  GNU General Public License for more details.
C
C  You should have received a copy of the GNU General Public License
C  along with this program; if not, write to the Free Software
C  Foundation, Inc., 51 Franklin Street, Fifth Floor,
C  Boston, MA  02110-1301  USA
C
C  François Méot <fmeot@bnl.gov>
C  Brookhaven National Laboratory
C  C-AD, Bldg 911
C  Upton, NY, 11973, USA
C  -------
      SUBROUTINE HIST
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
C     ---------------------------------
C     CONSTITUE UN TABLEAU POUR LISTING
C     LE TABLEAU EST LISTE PAR HISTO
C     ---------------------------------
      INCLUDE "C.CDF.H"     ! COMMON/CDF/ IES,LF,LST,NDAT,NRES,NPLT,NFAI,NMAP,NSPN,NLOG
      INCLUDE "MAXTRA.H"
      INCLUDE "C.DESIN.H"     ! COMMON/DESIN/ FDES(7,MXT),IFDES,KINFO,IRSAR,IRTET,IRPHI,NDES
      INCLUDE 'MXLD.H'
      INCLUDE "C.DON.H"     ! COMMON/DON/ A(MXL,MXD),IQ(MXL),IP(MXL),NB,NOEL
C      PARAMETER (LNTA=132) ; CHARACTER(LNTA) TA
C      PARAMETER (MXTA=45)
      INCLUDE "C.DONT.H"     ! COMMON/DONT/ TA(MXL,MXTA)
      INCLUDE "MAXCOO.H"
      LOGICAL AMQLU(5),PABSLU
      INCLUDE "C.FAISC.H"     ! COMMON/FAISC/ F(MXJ,MXT),AMQ(5,MXT),DP0(MXT),IMAX,IEX(MXT),
      CHARACTER(1) LET
      INCLUDE "C.FAISCT.H"     ! COMMON/FAISCT/ LET(MXT)
      INCLUDE "C.HISTO.H"     ! COMMON/HISTO/ ICTOT(JH,KH),MOYC(JH,KH) ,CMOY(JH,KH),JMAX(JH,KH)
      INCLUDE "C.HISTOG.H"     ! COMMON/HISTOG/NC(JH,120,KH),NH,XMI(JH,KH),XMO(JH,KH),XMA(JH,KH)
      INCLUDE "C.OBJET.H"     ! COMMON/OBJET/ FO(MXJ,MXT),KOBJ,IDMAX,IMAXT,KZOB
      INCLUDE "C.REBELO.H"   ! COMMON/REBELO/ NRBLT,IPASS,KWRT,NNDES,STDVM
      INCLUDE "C.SPIN.H"     ! COMMON/SPIN/ KSPN,KSO,SI(4,MXT),SF(4,MXT)

      DIMENSION XMO2(JH,KH), IMX(JH,KH)
      CHARACTER(2) TYP(JH,KH)
      CHARACTER(38) TEXT
      CHARACTER(1) KAR
      CHARACTER(14) NORME(2)
      CHARACTER(6) KOORD(JH), KUNIT(JH)

      DATA NORME /'NORMALISE     ','NON  NORMALISE'/
      DATA KOORD /'  D  ','Y    ','THETA','Z    ','PHI  ','  S  '
     >,4*' Time ','  Do ','Yo   ','To   ','Zo   ','PHIo ','  So '
     >,4*' Time ','Sx   ','Sy   ','Sz   ','<S>  ' /
      DATA KUNIT /'      ',' (CM) ',' (MRD)',' (CM) ',' (MRD)',' (CM) '
     >,4*' (s)  ','      ',' (CM) ',' (MRD)',' (CM) ',' (MRD)',' (CM) '
     >,8*' (s)  ' /
      DATA MIDCOL/ 60 /

C                   NUM.COORD.  BORNES      NBIN/LISTING     NUMERO DE
C                     1-JH     PHYSIQUES     \leq 120         L'HISTO
C                              COMPTAGE                        1-KH
C      READ(NDAT,*)    J     , HMI,HMA,        NBIN        ,    NH
      J = NINT(A(NOEL,1))
      HMI = A(NOEL,2)
      HMA = A(NOEL,3)
      NBIN = NINT(A(NOEL,4))
      NH = NINT(A(NOEL,5))
      KPR = 0
      IF(TA(NOEL,3) .EQ. 'PRINT') KPR = 1
C                                                     COMPTE LES S(SEC),
C                   AMPLI.VERT.  SYMBOLE  NORM.VERT   P(PRIM) OU Q (PAS
C                    #30                    1-2       DE SELECTION)
C      READ(NDAT,*) NBL    ,   KAR   ,  NRMY  ,       TYP(J,NH)
      NBL = NINT(A(NOEL,10))
      KAR = TA(NOEL,1)(1:1)
      NRMY = NINT(A(NOEL,11))
      TYP(J,NH) = TA(NOEL,2)(1:2)
C
C     LE GRAPHIQ SE CENTRE AUTOMATIQT SUR LA COLONNE
C     MIDCOL DU LSTING. SA LARGEUR EST DE NBIN COLONNES
C     POUR DES BORNES PHYSIQUES DE VALEURS HMI ET HMA

      IF(IPASS .EQ. 1) THEN
        IMX(J,NH)=0
        XMO2(J,NH)=0D0
        JMAX(J,NH) = 0
      ENDIF

      IF(NBIN.GT.2*MIDCOL) NBIN=2*MIDCOL
      FNORM=DBLE(NBIN)/(HMA-HMI)
      NC2=NBIN/2
      IC1=MIDCOL-NC2+1
C FM. Dec 2019
C      IC2=IC1+NBIN
      IC2=IC1+NBIN -1
      JMAX(J,NH) = JMAX(J,NH) + IMAX

C     ** LIMITES ET MOYENNE PHYS. DE LA VARIABLE :
      DO I=1,IMAX

C       +++ IEX<-1<=> PARTICULE STOPPEE
        IF(IEX(I) .GE. -1) THEN
          IF    (J .LE.  6) THEN
C            p/p_current, Y, T, Z, P, S
            FJI = F(J,I)
          ELSEIF(J .LE. 16) THEN
C            p/p_objet, Yo, To, Zo, Po, So
            FJI = FO(J-10,I)
          ELSEIF(J .LE. 24) THEN
C            COMPOSANTES SPIN: SX, SY, SZ, ET |S|=SQRT(SX2+SY2+SZ2)
            FJI = SF(J-20,I)
          ENDIF

          IF(  (TYP(J,NH) .EQ. 'P' .AND. LET(I) .NE. 'S')
     >    .OR. (TYP(J,NH) .EQ. 'S' .AND. LET(I) .EQ. 'S')
     >    .OR. (TYP(J,NH) .EQ. 'Q')  ) THEN
            IF(FJI .LT. XMI(J,NH)) XMI(J,NH) = FJI
            IF(FJI .GT. XMA(J,NH)) XMA(J,NH) = FJI
            IMX(J,NH) = IMX(J,NH) + 1
            XMO(J,NH) = XMO(J,NH) + FJI
            XMO2(J,NH) = XMO2(J,NH) + FJI*FJI
          ENDIF
        ENDIF
      ENDDO

C     ** COMPTAGE DANS LA FENETRE  HMI-HMA
      DO I=1,IMAX

C       +++ IEX<-1<=> PARTICULE STOPPEE
        IF(IEX(I) .GE. -1) THEN

          IF    (J .LE.  6) THEN
C           ** DP/P initial, Y, T, Z, P, S
            FJI = F(J,I)
          ELSEIF(J .LE. 16) THEN
C           ** DP/P final Yo, To, Zo, Po, So
            FJI = FO(J-10,I)
          ELSEIF(J .LE. 24) THEN
C           ** SPIN: SX, SY, SZ, ET <S>=SQRT(SX2+SY2+SZ2)
            FJI = SF(J-20,I)
          ENDIF

          IF(  (TYP(J,NH) .EQ. 'P' .AND. LET(I) .NE. 'S')
     >    .OR. (TYP(J,NH) .EQ. 'S' .AND. LET(I) .EQ. 'S')
     >    .OR. (TYP(J,NH) .EQ. 'Q')  ) THEN
             IF(FJI .GE. HMI .AND. FJI .LE. HMA) THEN
               ICOL = IC1 + NINT( ( FJI - HMI ) * FNORM )
               NC(J,ICOL,NH) = NC(J,ICOL,NH) + 1
             ENDIF
          ENDIF
        ENDIF
      ENDDO

      IF(NRES.GT.0) THEN
        WRITE(NRES,106) KOORD(J)
 106    FORMAT(/,30X,'HISTOGRAMME  DE  LA  COORDONNEE  ',A)
        IF     (TYP(J,NH) .EQ. 'P') THEN
          TEXT='PARTICULES  PRIMAIRES'
        ELSEIF (TYP(J,NH) .EQ. 'S') THEN
          TEXT='PARTICULES  SECONDAIRES'
        ELSEIF (TYP(J,NH) .EQ. 'Q') THEN
          TEXT='PARTICULES  PRIMAIRES  ET  SECONDAIRES'
        ENDIF
        WRITE(NRES,104) TEXT
 104    FORMAT(30X,A)

        WRITE(NRES,103) HMI,HMA,KUNIT(J)
 103    FORMAT(30X,'DANS  LA  FENETRE : '
     >  ,1P,G12.4,' / ',G12.4,A)
        IF(KPR.EQ.1) WRITE(NRES,FMT='(30X,A)')
     >  'Request to print out histogram to zgoubi.HISTO.out.'
        WRITE(NRES,FMT='(A)') ' '
             
        CALL HISTAB(IC1,IC2,J,NBL,KAR,NRMY,
     >                                     IER)

        IF(KPR .EQ. 1) CALL HISTA2(HMI,HMA,IC1,IC2,J,NH,NOEL)        
        
        IF(IER .NE. 1) THEN
          POSIT=(MOYC(J,NH)-IC1)/FNORM + HMI
          WRITE(NRES,102) POSIT,KUNIT(J), 1.D0/FNORM,KUNIT(J)
 102      FORMAT(15X,' VAL. PHYS. AU  "      "         : ',1P,G10.3,A6,/
     >    ,15X,' RESOLUTION  PAR  BIN          : ',   G10.3,A6)
        ENDIF
       
c 5      CONTINUE

        SIGMA=SQRT(XMO2(J,NH)/IMX(J,NH)-(XMO(J,NH)/IMX(J,NH))**2)
        WRITE(NRES,105) IMX(J,NH),IC2-IC1+1,IC1,IC2,
     >  XMI(J,NH),XMA(J,NH),XMA(J,NH)-XMI(J,NH),KUNIT(J),
     >  (HMA-HMI)/(IC2-IC1+1),
     >  XMO(J,NH)/IMX(J,NH), KUNIT(J), SIGMA,KUNIT(J)
 105    FORMAT(/,15X,' PARAMETRES  PHYSIQUES  DE  LA  DISTRIBUTION :'
     >  ,/,20X, 'COMPTAGE = ',I7,'  PARTICULES'
     >  ,/,20X, 'NOMBRE DE BINS = ',I7,'  (IC1 - IC2 : ',I0,' - ',I0,')'
     >  ,/,20X,1P,'Found min. = ',G12.4,', found max. = '
     >  ,G12.4,', MAX-MIN = ',G12.4,A
     >  ,', DX PAR BIN = ',G12.4
     >  ,/,20X,   'MOYENNE = ',G12.4,A
     >  ,/,20X,   'SIGMA = ',G12.4,A,/)
        WRITE(NRES,101) IEX(1),(F(J,1),J=1,7)
  101   FORMAT(' TRAJ 1 IEX,D,Y,T,Z,P,S,time :',I3,1P,5G12.4,2G17.5)

      ELSE

        IF(KPR .EQ. 1) CALL HISTA2(HMI,HMA,IC1,IC2,J,NH,NOEL)
         
      ENDIF

      
      RETURN
      END
