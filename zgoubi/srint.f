C  ZGOUBI, a program for computing the trajectories of charged particles
C  in electric and magnetic fields
C  Copyright (C) 1988-2007  François Méot
C
C  This program is free software; you can redistribute it and/or modify
C  it under the terms of the GNU General Public License as published by
C  the Free Software Foundation; either version 2 of the License, or
C  (at your option) any later version.
C
C  This program is distributed in the hope that it will be useful,
C  but WITHOUT ANY WARRANTY; without even the implied warranty of
C  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C  GNU General Public License for more details.
C
C  You should have received a copy of the GNU General Public License
C  along with this program; if not, write to the Free Software
C  Foundation, Inc., 51 Franklin Street, Fifth Floor,
C  Boston, MA  02110-1301  USA
C
C  François Méot <fmeot@bnl.gov>
C  Brookhaven National Laboratory
C  C-AD, Bldg 911
C  Upton, NY, 11973, USA
C  -------
      SUBROUTINE SRINT(DS,IMAX)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE "C.CDF.H"     ! COMMON/CDF/ IES,LF,LST,NDAT,NRES,NPLT,NFAI,NMAP,NSPN,NLOG
      INCLUDE "C.CHAVE_2.H"     ! COMMON/CHAVE/ B(5,3),V(5,3),E(5,3)
      INCLUDE "C.CONST.H"     ! COMMON/CONST/ CL9,CL ,PI,RAD,DEG,QE ,AMPROT, CM2M
      PARAMETER (LBLSIZ=20)
      CHARACTER(LBLSIZ) LABEL
      INCLUDE 'MXLD.H'
C      INCLUDE 'MXFS.H'
      INCLUDE "C.LABEL.H"     ! COMMON/LABEL/ LABEL(MXL,2)
      INCLUDE "C.PTICUL.H"     ! COMMON/PTICUL/ AM,Q,G,TO
      INCLUDE "C.REBELO.H"   ! COMMON/REBELO/ NRBLT,IPASS,KWRT,NNDES,STDVM
      INCLUDE "C.RIGID.H"     ! COMMON/RIGID/ BORO,DPREF,HDPRF,DP,QBR,BRI

      DIMENSION SRI(6,2), SRIO(6,2)
      SAVE SRI
      
      SAVE UNIT,UNITE
      DIMENSION F0(6,6)

C      DIMENSION TEMP(MXJ,MXT)
      
      DATA UNIT,UNITE / 1.D-2, 1.D-6/

      CALL OPTIC3(
     >            F0)
      
C----- Curvature (/m)
      CURVY=SQRT(B(1,1)*B(1,1)+B(1,2)*B(1,2)+B(1,3)*B(1,3)) / UNIT

C SRI(1-6,Y-Z):
      btY = F0(1,1) ; btZ = F0(3,3)
      alfY = -F0(2,1) ; alfZ = -F0(4,3)
      DY = F0(1,6) ; DZ = F0(3,6)
      DPY = F0(2,6) ; DPZ = F0(4,6)

c      write(*,*) '1/rho, dY, dpY :',curvy,dY,dpY,alfY,btY,ipass
C     read(*,*)
      
      SRI(1,1) = SRI(1,1) + DS*UNIT *CURVY *DY
      C2 = CURVY*CURVY
      C3 = C2 * CURVY
      SRI(2,1) = SRI(2,1) + DS*UNIT *C2
      SRI(3,1) = SRI(3,1) + DS*UNIT *abs(C3)
      SRI(4,1) = SRI(4,1) + DS*UNIT * (1.D0 -2.D0*D2B) *DY *C3
C      H1= F0(2,2)*DY*DY+2.D0*(-F0(1,2))*DY*DPY+F0(1,1)*DPY*DPY   ! F012= -alpha 
      HY= (DY*DY+(bty*DPY + alfy*DY)**2)/bty ! F012= -alpha
C      write(*,*) 'H  H  :',h1, hY
C            read(*,*)      
      SRI(5,1) = 0.D0  ! SRI(5,1) + DS*UNIT *HY *ABS(C3)
      SRI(6,1) = 0.d0

c      write(88,*) 'srint 1-6 : ',
c     > sri(1,1),sri(2,1),sri(3,1),sri(4,1),sri(5,1),ipass
      
      RETURN

      ENTRY SRINT0
      SRI(:,:) = 0.D0
      RETURN

      ENTRY SRINT1(
     >             SRIO)
      SRIO(:,:) = SRI(:,:)
      RETURN
      END

