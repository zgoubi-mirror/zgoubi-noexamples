C  ZGOUBI, a program for computing the trajectories of charged particles
C  in electric and magnetic fields
C  Copyright (C) 1988-2007  François Méot
C
C  This program is free software; you can redistribute it and/or modify
C  it under the terms of the GNU General Public License as published by
C  the Free Software Foundation; either version 2 of the License, or
C  (at your option) any later version.
C
C  This program is distributed in the hope that it will be useful,
C  but WITHOUT ANY WARRANTY; without even the implied warranty of
C  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C  GNU General Public License for more details.
C
C  You should have received a copy of the GNU General Public License
C  along with this program; if not, write to the Free Software
C  Foundation, Inc., 51 Franklin Street, Fifth Floor,
C  Boston, MA  02110-1301  USA
C
C  François Méot <fmeot@bnl.gov>
C  Brookhaven National Laboratory
C  C-AD, Bldg 911
C  Upton, NY, 11973, USA
C  -------
      SUBROUTINE HISTAB(IC1,IC2,JIN,NLIN,KAR,NORM,
     >                                            IER)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
C     -------------------------------------------------------------
C     Y EST UN HISTOGRAMME DE VALEURS
C     ON TRACE Y SUR LISTING, DANS LES LIMITES UTILISATEUR
C       IC1, IC2( <120 ) EN BIN , XMIN, XMAX EN PHYSIQ.
C     LA FENETRE 'ECRAN' EST IC1-IC2 (NUMEROS DE COLONNES) EN
C       HORIZONTAL, ET NLIN (NOMBRE DE LIGNES LISTING) EN VERTICAL
C     NORM=0 (PAS DE NORMALISTION) OU NORM=1 (NORMALISATION /NCMAX)
C     -------------------------------------------------------------

      INCLUDE "C.CDF.H"     ! COMMON/CDF/ IES,LF,LST,NDAT,NRES,NPLT,NFAI,NMAP,NSPN,NLOG
      INCLUDE "C.HISTO.H"     ! COMMON/HISTO/ ICTOT(JH,KH),MOYC(JH,KH) ,CMOY(JH,KH),JMAX(JH,KH)
      INCLUDE "C.HISTOG.H"     ! COMMON/HISTOG/NC(JH,120,KH),NH,XMI(JH,KH),XMO(JH,KH),XMA(JH,KH)
      INCLUDE "C.REBELO.H"   ! COMMON/REBELO/ NRBLT,IPASS,KWRT,NNDES,STDVM

      PARAMETER(I130=130)
      DIMENSION ISTO(I130)
      CHARACTER(1) KARL(I130),KARO(10),KAR,BLANC

      LOGICAL OKOPN, IDLUNI
      SAVE OKOPN
      SAVE LW
      
      DATA KARO / '1','2','3','4','5','6','7','8','9','0'/
      DATA BLANC/ ' '/
C     ** numero de la 1-er colonne sur  le listing
      DATA IC0/ 10 /
      DATA OKOPN / .FALSE. /
      
      IER = 0
      J = JIN
      
      IC10=IC0+IC1
      IC20=IC0+IC2

      IF(IC20 .GT. I130)
     >CALL ENDJOB('Pgm histab. Re-size arryas to > ',130)
      
C     *** COMPTAGE, MOYENNE
      CMOY(J,NH)=0D0
      MOYC(J,NH)=0
      ICTOT(J,NH)=0
      DO IC=IC1,IC2
        ICTOT(J,NH)=ICTOT(J,NH) + NC(J,IC,NH)
        CMOY(J,NH) = CMOY(J,NH) + IC*NC(J,IC,NH)
      ENDDO
      
      IF(ICTOT(J,NH) .GT. 0) THEN
        MOYC(J,NH) = NINT(CMOY(J,NH)/ICTOT(J,NH))

        DO IC=IC1,IC2
          ISTO(IC+IC0)=NC(J,IC,NH)
        ENDDO
       
        IF(NORM .EQ. 1) THEN
C         ** NORMALISE NC (DANS LA FENETRE IC1-IC2)
          YMAX=0D0
          DO IC=IC1,IC2
            IF(YMAX .LT. NC(J,IC,NH)) YMAX=NC(J,IC,NH)
          ENDDO
          IF(YMAX .NE. 0.D0) THEN
            DO  IC=IC10,IC20
              ISTO(IC) = INT(.9D0 * ISTO(IC) * NLIN/YMAX)
            ENDDO
c 3            ISTO(IC) = .9D0 * ISTO(IC) * NLIN/YMAX
          ENDIF
        ENDIF

C       ** TRACE L'HISTO
        DO LINE=NLIN,1,-1
          DO IC=IC10,IC20
            IF(ISTO(IC) .GE. LINE) THEN
              IF( (LINE/10)*10 .EQ. LINE ) THEN
                KARL(IC)='0'
              ELSE
                KARL(IC)=KAR
              ENDIF
            ELSE
              KARL(IC)=' '
            ENDIF
          ENDDO
          WRITE(NRES,100) LINE, (BLANC,IC=6,IC10-1)
     >    , (KARL(JC),JC=IC10,IC20)
 100      FORMAT(I5,126A1)
        ENDDO

        WRITE(NRES,103)
     >  (BLANC,II=1,IC10-1),(KARO(I-(I/10)*10+1),I=IC10-1,IC20-1)
 103    FORMAT(/,131A1)
        WRITE(NRES,107) (BLANC,II=1,(IC10/10)*10-1)
     >  ,( ( BLANC,II=(I-(I/10)*10),8 )
     >  ,KARO((I)/10) , I=(IC10/10)*10,(IC20/10-1 )*10-1,10)
 107    FORMAT(1X,131A1)

        WRITE(NRES,104) ICTOT(J,NH),JMAX(J,NH)
     >  ,MOYC(J,NH),NC(J,MOYC(J,NH),NH)
 104    FORMAT(
     >  //,15X,' TOTAL  COMPTAGE  TOUS  BINS     : ',I7,
     >  '  PARTICULES  SUR  UN  TOTAL  DE  ',I7,'  LANCEES'
     >  ,/,15X,' NUMERO   DU  BIN  MOYEN       : ',I7
     >  ,/,15X,' COMPTAGE  AU   "      "         : ',I7 )

      ELSEIF(ICTOT(J,NH) .EQ. 0) THEN

        WRITE(NRES,108)
 108    FORMAT(////,15X,' TOTAL  COMPTAGE  TOUS  BINS     : 0',///)
        IER = 1

      ENDIF

      RETURN

      ENTRY HISTA2(HMI,HMA,IIC1,IIC2,JJ,NNH,NUEL)

C      IF(KPR .EQ. 1) THEN
        IF(.NOT. OKOPN) THEN
          OKOPN = (IDLUNI(
     >                    LW))
          IF(.NOT. OKOPN) CALL ENDJOB
     >    ('Pgm histab. Cannot get free unit for object file',-99)
C          IF(NRES.GT.0) WRITE(NRES,FMT='(/,''   Opening output file  '',
C     >    A,'',  in logical unit # : '',I3)')
C     >    'zgoubi.HISTO.out ',LW
          OPEN(UNIT=LW,FILE='zgoubi.HISTO.out',ERR=96,IOSTAT=IOS)
          WRITE(LW,
     >    FMT='(A,T9,A,T19,A,T31,A,T45,A,T55,A,T65,A,T75,A,T85,A,
     >    T95,A,T105,A,T115,A,T125,A)')
     >    '#','   1','   2','   3','   4','   5','   6','   7',
     >    '   8','   9','  10','  11','  12'
          WRITE(LW,
     >    FMT='(A,T9,A,T19,A,T31,A,T45,A,T55,A,T65,A,T75,A,T85,A,
     >    T95,A,T105,A,T115,A,T125,A)')
     >    '#','   X','HIST',' HDX','SHDX',' S_H','  IC',' IC1',
     >    ' IC2','NH','NOEL','Pass','Coor' 
          OKOPN = .TRUE. 
        ENDIF
        
        WRITE(LW,FMT='(A1,I0)') '#',NNH
        
        DX = (HMA-HMI)/DBLE((IIC2-IIC1+1))
        II = 0
        MY = 0.D0
        SYDX = 0.D0
        DO IC=IIC1,IIC2
          YY = DBLE(NC(JJ,IC,NNH))
          MY = MY + NC(JJ,IC,NNH)
          YDX = YY * DX
          SYDX = SYDX + YDX
          WRITE(LW,FMT='(E14.6,1X,I7,2(E12.4,2X),8(I8,2X))')
     >    HMI+DX*DBLE(II), NC(JJ,IC,NNH), YDX, SYDX, MY,
     >    IC,IIC1,IIC2,NNH,NUEL,IPASS,JJ
          II = II + 1
        ENDDO         
C      ENDIF
      
      RETURN

 96   CONTINUE
      CALL ENDJOB
     >('Pgm histab. Could not open file zgoubi.HISTO.out',-99)
      RETURN
      END
