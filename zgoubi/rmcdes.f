C  ZGOUBI, a program for computing the trajectories of charged particles
C  in electric and magnetic fields
C  Copyright (C) 1988-2007  François Méot
C
C  This program is free software; you can redistribute it and/or modify
C  it under the terms of the GNU General Public License as published by
C  the Free Software Foundation; either version 2 of the License, or
C  (at your option) any later version.
C
C  This program is distributed in the hope that it will be useful,
C  but WITHOUT ANY WARRANTY; without even the implied warranty of
C  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C  GNU General Public License for more details.
C
C  You should have received a copy of the GNU General Public License
C  along with this program; if not, write to the Free Software
C  Foundation, Inc., 51 Franklin Street, Fifth Floor,
C  Boston, MA  02110-1301  USA
C
C  François Méot <fmeot@bnl.gov>
C  Brookhaven National Laboratory
C  C-AD, Bldg 911
C  Upton, NY, 11973
C  USA
C  -------
      SUBROUTINE RMCDES
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE "C.CDF.H"     ! COMMON/CDF/ IES,LF,LST,NDAT,NRES,NPLT,NFAI,NMAP,NSPN,NLOG
      INCLUDE 'MXLD.H'
      INCLUDE "C.DON.H"         ! COMMON/DON/ A(MXL,MXD),IQ(MXL),IP(MXL),NB,NOEL
      
      CHARACTER(132) TXT
      CHARACTER(30) STRA(5)

      INTEGER DEBSTR
      LOGICAL STRCON
      PARAMETER (KSIZ=10)
      CHARACTER(KSIZ) KLE

      LINE = 1
      READ(NDAT,FMT='(A)') TXT
      TXT = TXT(DEBSTR(TXT):)
      IF(STRCON(TXT,'!',
     >                   IS)) TXT = TXT(:IS-1)
C FM - Dec 2019. Allow 'INFO' anywhere on the line, rather than at begining, before.
C     IF(TXT(1:4).EQ.'INFO') THEN
      IF(STRCON(TXT,'INFO',
     >                     IS)) THEN
        A(NOEL,5)=1
        IF(IS .GT. 2) THEN
           TXT = TXT(1:IS-1)//TXT(IS+4:)
        ELSE
          GOTO 90
        ENDIF
      ELSE
        A(NOEL,5)=0.D0
      ENDIF

C----- Read M1, M2
      CALL STRGET(TXT,5
     >                 ,MST,STRA)
      READ(TXT,*,ERR=90,END=90) (A(NOEL,I),I=1,2)
C----- Read life-time
      IF(MST.GT.2) READ(stra(3),*,ERR=90,END=90) A(NOEL,3)
C----- Read value of back option
      IF(MST.GT.3) READ(stra(4),*,ERR=90,END=90) A(NOEL,4)
C      GOTO 11
C 10   CONTINUE
C      A(NOEL,3) = 0.D0
C      A(NOEL,4) = 0.D0  ! 1 here, to set bacward to .true. in mcdes.
C----- Seeds
      LINE = LINE + 1
      READ(NDAT,*,ERR=90,END=90) (A(NOEL,I),I=10,12)

      RETURN

 90   CONTINUE
      CALL ZGKLEY(
     >            KLE)
      CALL ENDJOB('*** Pgm robjet, keyword '//KLE//' : '//
     >'input data error, at line #',LINE)
      RETURN

      END
