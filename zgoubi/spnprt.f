C  ZGOUBI, a program for computing the trajectories of charged particles
C  in electric and magnetic fields
C  Copyright (C) 1988-2007  François Méot
C
C  This program is free software; you can redistribute it and/or modify
C  it under the terms of the GNU General Public License as published by
C  the Free Software Foundation; either version 2 of the License, or
C  (at your option) any later version.
C
C  This program is distributed in the hope that it will be useful,
C  but WITHOUT ANY WARRANTY; without even the implied warranty of
C  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C  GNU General Public License for more details.
C
C  You should have received a copy of the GNU General Public License
C  along with this program; if not, write to the Free Software
C  Foundation, Inc., 51 Franklin Street, Fifth Floor,
C  Boston, MA  02110-1301  USA
C
C  François Méot <fmeot@bnl.gov>
C  Brookhaven National Laboratory
C  C-AD, Bldg 911
C  Upton, NY, 11973, USA
C  -------
      SUBROUTINE SPNPRT(LBL1, LBL2)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      PARAMETER (LBLSIZ=20)
      CHARACTER(LBLSIZ) LBL1, LBL2
      INCLUDE "C.CDF.H"     ! COMMON/CDF/ IES,LF,LST,NDAT,NRES,NPLT,NFAI,NMAP,NSPN,NLOG
      INCLUDE "C.CONST.H"     ! COMMON/CONST/ CL9,CL ,PI,RAD,DEG,QE ,AMPROT, CM2M
      INCLUDE "MAXTRA.H"
      INCLUDE "MAXCOO.H"
      LOGICAL AMQLU(5),PABSLU
      INCLUDE 'MXLD.H'
      INCLUDE "C.DON.H"     ! COMMON/DON/ A(MXL,MXD),IQ(MXL),IP(MXL),NB,NOEL
      INCLUDE "C.FAISC.H"     ! COMMON/FAISC/ F(MXJ,MXT),AMQ(5,MXT),DP0(MXT),IMAX,IEX(MXT),
C     $     IREP(MXT),AMQLU,PABSLU
      CHARACTER(1) LET
      INCLUDE "C.FAISCT.H"     ! COMMON/FAISCT/ LET(MXT)
      INCLUDE "C.OBJET.H"     ! COMMON/OBJET/ FO(MXJ,MXT),KOBJ,IDMAX,IMAXT,KZOB
      INCLUDE "C.PTICUL.H"     ! COMMON/PTICUL/ AM,Q,G,TO
      INCLUDE "C.REBELO.H"   ! COMMON/REBELO/ NRBLT,IPASS,KWRT,NNDES,STDVM
      INCLUDE "C.RIGID.H"     ! COMMON/RIGID/ BORO,DPREF,HDPRF,DP,QBR,BRI
      INCLUDE "C.SPIN.H"     ! COMMON/SPIN/ KSPN,KSO,SI(4,MXT),SF(4,MXT)

C      DIMENSION SMI(4,MXT), SMA(4,MXT)
      LOGICAL IDLUNI

C      DIMENSION SPMI(4,MXT), SPMA(4,MXT)
      PARAMETER (ICMXT=4*MXT)

      DIMENSION AA(3),BB(3)  !,XX(3)
      DIMENSION PHI(MXT), PHYZ(MXT)
C      SAVE SXMF, SYMF, SZMF

      INTEGER DEBSTR, FINSTR
      LOGICAL FIRST
      SAVE LUNPRT, FIRST

      DIMENSION SMAT(3,3)
c      LOGICAL FITIN

      LOGICAL OKMAT

      PARAMETER (I3=3)       
      
C      DATA SXMF, SYMF, SZMF /  3 * 0.D0 /
C      DATA SPMI, SPMA / ICMXT*1D10, ICMXT* -1D10 /
      DATA FIRST / .TRUE. /
      DATA TR1, TR2, TR3 / 3 * 0.D0 /
      DATA OKMAT / .FALSE. /

      OKMAT= LBL1(DEBSTR(LBL1):FINSTR(LBL1)) .EQ. 'MATRIX'
     >  .OR. LBL2(DEBSTR(LBL2):FINSTR(LBL2)) .EQ. 'MATRIX'

      IF(OKMAT) THEN
         
        JDMAX=IDMAX
        IF(IMAX .LT. I3*JDMAX) JDMAX = IMAX/IDMAX

        IF(NRES.GT.0) THEN
          IF(JDMAX .GE. 1) WRITE(NRES,121) JDMAX
 121      FORMAT(25X,' -- ',I0,'  GROUPS  OF  MOMENTA  FOLLOW   --')
        ELSE
          CALL ENDJOB('Sbr spnprt. Minimal number of particles required'
     >    //' for matrix computation is ',3)  
        ENDIF

        IID = 0
        DO 3 ID=1,JDMAX

          IID = IID + 1
         
          IMAX1=1+(ID-1)* I3
C          IMAX2=IMAX1+JMAXT-1
          IMAX2=IMAX1+2

          IF(NRES.GT.0) THEN
            WRITE(NRES,120) ID,F(1,3*ID-2),IMAX1, IMAX2
 120        FORMAT(/,20X,82('-')
     >      ,/,20X,'Momentum  group  #',I0,' (D= ',1P,E14.6,0P,
     >      ';  particles  ',I0,'  to ',I0,' ; ')

            WRITE(NRES,110) I3
 110        FORMAT(//,15X,' Spin  components  of  the '
     >      ,I6,'  particles,  spin  angles :'
     >      ,//,T20,'INITIAL',T70,'FINAL'
     >      ,//,T12,'SX',T22,'SY',T32,'SZ',T42,'|S|'
     >      ,T54,'SX',T64,'SY',T74,'SZ',T84,'|S|',T94,'GAMMA'
     >      ,T102,'|Si,Sf|',T112,'(Z,Sf_yz)',T122,'(Z,Sf)')
            WRITE(NRES,FMT='(
     >      T102,'' (deg.)'',T112,'' (deg.)'',T122,'' (deg.)'')')
            WRITE(NRES,fmt='(T92,a,/)')
     >           '(Sf_yz : projection of Sf on YZ plane)'

            DO I=IMAX1,IMAX2
              IF( IEX(I) .GE. -1 ) THEN
                P = BORO*CL9 *F(1,I) *Q
                GAMA = SQRT(P*P + AM*AM)/AM
                AA(1) = SI(1,I)
                AA(2) = SI(2,I)
                AA(3) = SI(3,I)
                BB(1) = SF(1,I)
                BB(2) = SF(2,I)
                BB(3) = SF(3,I)

C Angle between SI and SF
                CPHI = VSCAL(AA,BB,3) /XNORM(AA,3) /XNORM(BB,3)
                IF(ABS(CPHI) .GT.  1.D0) THEN
                  PHI(I) = 0.D0
                ELSE
                  PHI(I) = SIGN(ACOS(CPHI) * DEG , CPHI)
                ENDIF

C (Z,Sf_yz): Angle between 'projection of SF on YZ plane' and Z axis
C              PHYZ(I) = ATAN2(BB(2), BB(3)) * DEG
                PHYZ(I) =
     >          ATAN2(SQRT(BB(2)**2+BB(3)**2)/XNORM(BB,3),BB(3)) * DEG
              
C (Z,Sf): Angle between SF and Z axis = acos(S.Z) /|S|.|Z|  = acos(S_Z)
                PHIZF = ACOS( BB(3)/XNORM(BB,3)) * DEG

                WRITE(NRES,101) LET(I),IEX(I),(SI(J,I),J=1,4)
C     >        ,(SF(J,I),J=1,4),CPHI,PHI(I),PHYZ(I),PHIZF,I
     >          ,(SF(J,I),J=1,4),GAMA,PHI(I),PHYZ(I),PHIZF,I
 101            FORMAT(1X,A1,1X,I2,4(1X,F9.6),3X,4(1X,F9.6),1X,F11.4,
     >          3(1X,F8.3),1X,I4)

              ENDIF
            ENDDO

          ENDIF

C Matrix computation
C Pattern in OBJET has to be
C  3 identical particles on orbit with their spin components resp. 0,0,1, 0,1,0, 1,0,0

          CALL SPNMAT(ID,
     >                   SMAT,TRM, SROT,TR1,TR2,TR3,QS)

          IF(NRES.GT.0) THEN
C              A2XY = ATAN2(TR3,SQRT(TR1*TR1+TR2*TR2))*DEG
            RXY = SQRT(TR1*TR1+TR2*TR2)
C              A2XY = ATAN2(TR3,RXY)*DEG
            A2Z = ATAN2(RXY,TR3)*DEG  !  also  A2Z = ACOS(TR3)*DEG

c              write(88,*) ' spnprt a2z,  a2z2 : ',a2z,a2z2
c              write(88,*) 
              
            A2XY = (PI/2.D0 - ATAN2(RXY,TR3) ) *DEG
C            A2X = ATAN2(TR2,TR1)*DEG
            A2X = acos(tr1) * deg
            DETS = GETDET(SMAT,3)
            WRITE(NRES,103) ID
 103        FORMAT(//,18X,'Spin transfer matrix, momentum group # '
     >      ,I0,' :',/)
            WRITE(NRES,104) (( SMAT(IA,IB) , IB=1,3) , IA=1,3)
 104        FORMAT(6X,1P,3G16.6)
            WRITE(NRES,112) DETS,TRM,SROT*DEG,TR1,TR2,TR3,A2XY,A2X,QS
 112        FORMAT(
     >      /,5X,'Determinant = ',F18.10,/,5X,'Trace = ',F18.10,
     >      ';   spin precession acos((trace-1)/2) = ',F18.10,' deg',
     >      /,5X,'Precession axis :  (',F7.4,', ',F7.4,', ',F7.4,')',
     >      '  ->  angle to (X,Y) plane, to X axis : ',
     >      F10.4,', ',F10.4,' deg',/,
     >      5X,'Spin precession/2pi (or Qs, fractional) :  ',1P,E12.4)
          ENDIF                   
       
 3      CONTINUE

        IF(3*JDMAX .LT. IMAX) WRITE(NRES,118) IMAX-JDMAX*I3
 118    FORMAT(/,20X,82('-'),/,20X,'Remaining  ',I0,'  particles  ; ')
      
      ELSE

        IMAX1 = 1 ; IMAX2 = IMAX
        IID = 0
        
      ENDIF  ! OKMAT
      
      IF (IMAX2 .GT. 1 .AND. IMAX .GT. IID*3) THEN

        IMAX1 = IID*3 + 1 
        IMAX2 = IMAX

        IF(NRES .GT. 0) THEN
C          WRITE(NRES,110) JMAXT
          WRITE(NRES,110) IMAX2 - IMAX1 + 1
          WRITE(NRES,FMT='(
     >    T102,'' (deg.)'',T112,'' (deg.)'',T122,'' (deg.)'')')
          WRITE(NRES,fmt='(T92,a,/)')
     >           '(Sf_yz : projection of Sf on YZ plane)'

          DO I=IMAX1,IMAX2
            IF( IEX(I) .GE. -1 ) THEN
              P = BORO*CL9 *F(1,I) *Q
              GAMA = SQRT(P*P + AM*AM)/AM
              AA(1) = SI(1,I)
              AA(2) = SI(2,I)
              AA(3) = SI(3,I)
              BB(1) = SF(1,I)
              BB(2) = SF(2,I)
              BB(3) = SF(3,I)

C Angle between SI and SF
              CPHI = VSCAL(AA,BB,3) /XNORM(AA,3) /XNORM(BB,3)
              IF(ABS(CPHI) .GT.  1.D0) THEN
                PHI(I) = 0.D0
              ELSE
                PHI(I) = SIGN(ACOS(CPHI) * DEG , CPHI)
              ENDIF

C (Z,Sf_yz): Angle between 'projection of SF on YZ plane' and Z axis
C              PHYZ(I) = ATAN2(BB(2), BB(3)) * DEG
              PHYZ(I) =
     >        ATAN2(SQRT(BB(2)**2+BB(3)**2)/XNORM(BB,3),BB(3)) * DEG
              
C (Z,Sf): Angle between SF and Z axis = acos(S.Z) /|S|.|Z|  = acos(S_Z)
              PHIZF = ACOS( BB(3)/XNORM(BB,3)) * DEG

              WRITE(NRES,101) LET(I),IEX(I),(SI(J,I),J=1,4)
     >        ,(SF(J,I),J=1,4),GAMA,PHI(I),PHYZ(I),PHIZF,I

            ENDIF
          ENDDO

C          WRITE(NRES,130) 
C          DO I=IMAX1,IMAX2
C            IF( IEX(I) .GE. -1 ) THEN
C              P = BORO*CL9 *F(1,I) *Q
C              GAMA = SQRT(P*P + AM*AM)/AM
C              WRITE(NRES,131) (SPMI(J,I),SPMA(J,I),J=1,4),F(1,I)
C     >           ,GAMA,I,IEX(I)
C            ENDIF
C          ENDDO

        ENDIF  ! NRES

      ENDIF  ! IMAX2 .GT. 1 .AND. IMAX .GT. IID*3

c      CALL FITSTA(5,
c     >              FITIN)
c      IF(.NOT. FITIN) THEN  
            
        IF  (LBL1(DEBSTR(LBL1):FINSTR(LBL1)) .EQ. 'PRINT'
     >  .OR. LBL2(DEBSTR(LBL2):FINSTR(LBL2)) .EQ. 'PRINT') THEN
           
          IF(FIRST) THEN
            FIRST = .FALSE.
            IF(IDLUNI(
     >                LUNPRT)) THEN
              OPEN(UNIT=LUNPRT,FILE='zgoubi.SPNPRT.Out',ERR=96)
            ELSE
              GOTO 96
            ENDIF
            WRITE(LUNPRT,FMT='(3(A,/),A)')
     >      '# spin data. PRINT by spnprt.f ',
     >      '# 1  2  3  4  5  6    7    8     9 10 11 12   '//
     >      '    13 14 15 16      17       18   19 '//
     >      '   20     21     22   23   24  25  26  27  28  29 '//
     >      '   30-32  33        34    35      36            '//
     >      '               '//
     >      '  37-45        46          47    48    49    50'//
     >      '         51         52            53             54', 
     >      '# Y, T, Z, P, S, D, TAG, IEX, (SI(J,I),J=1,4), '//
     >      '(SF(J,I),J=1,4), gamma, G.gamma, PHI, '//
     >      'PHYZ, ITRAJ, IPASS, NOEL, Yo, To, Zo, Po, So, Do, '//
     >      'AXE(1,3) Qs !spnprt.f '//
     >      ' LBL1    LBL2 '//
     >      ' spin matrix: s11,s12,...,s32,s33     '//
     >      'Trace  Spin prec.   TR1   TR2   TR3  Spin-tune  '//
     >      'n to Z-ax  n_pi to X-ax  ACOS(TR3)*DEG',
     >      '# cm mr cm mr cm  -   -     -    - - - -       '//
     >      '    - - - -            -         Spin angles in deg.'//
     >      '                  -         -           -          '//
     >      '     Qs !spnprt.f  -  - '
          ENDIF
           
          DO I=IMAX1,IMAX2
            IF( IEX(I) .GE. -1 ) THEN
              P = BORO*CL9 *F(1,I) *Q
              GAMA = SQRT(P*P + AM*AM)/AM
              RXY = SQRT(TR1*TR1+TR2*TR2)
C              A2XY = ATAN2(TR3,RXY)*DEG
              A2Z = ATAN2(RXY,TR3)*DEG !  also
              A2Z2 = ACOS(TR3)*DEG
              A2XY = ( PI/2.D0 - ATAN2(RXY,TR3) ) *DEG
C              A2X = ATAN2(TR2,TR1)*DEG
              A2X = acos(tr1) * deg
              WRITE(LUNPRT,111) (F(J,I),J=2,6),F(1,I)
     >        ,'''',LET(I),'''',IEX(I),(SI(J,I),J=1,4)
     >        ,(SF(J,I),J=1,4),GAMA,G*GAMA,PHI(I),PHYZ(I),I,IPASS
     >        ,NOEL,(FO(J,I),J=2,6),FO(1,I),TR1,TR2,TR3,QS
     >        ,'!spnprt.f',LBL1,LBL2
     >        ,(( SMAT(IA,IB),IB=1,3),IA=1,3),TRM,SROT*DEG
     >        ,TR1,TR2,TR3,QS,a2z, a2x, a2z2
 111          FORMAT(1X,1P,6(1X,E14.6),1X,3A1,1X,I2,12(1X,E16.8)
     >        ,3(1X,I6),9(1X,E16.8),1X,0P,F9.6,3(1X,A)
     >        ,18(1X,E14.6))
            ENDIF
          ENDDO
C Leaving unclosed allows stacking when combined use of FIT and REBELOTE
C        CLOSE(LUNPRT)
        ENDIF   ! PRINT
c      ENDIF  ! FITIN
      
      CALL FLUSH2(LUNPRT,.FALSE.)
      
      RETURN

 96   CONTINUE
      WRITE(ABS(NRES),FMT='(/,''SBR SPNPRT : '',
     >           ''Error open file zgoubi.SPNPRT.Out'')')
      WRITE(*        ,FMT='(/,''SBR SPNPRT : '',
     >           ''Error open file zgoubi.SPNPRT.Out'')')
      CALL ENDJOB('Pgm spnprt. Error open file zgoubi.SPNPRT.Out',-99)

      RETURN
      END
