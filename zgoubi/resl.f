C  ZGOUBI, a program for computing the trajectories of charged particles
C  in electric and magnetic fields
C  Copyright (C) 1988-2007  François Méot
C
C  This program is free software; you can redistribute it and/or modify
C  it under the terms of the GNU General Public License as published by
C  the Free Software Foundation; either version 2 of the License, or
C  (at your option) any later version.
C
C  This program is distributed in the hope that it will be useful,
C  but WITHOUT ANY WARRANTY; without even the implied warranty of
C  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C  GNU General Public License for more details.
C
C  You should have received a copy of the GNU General Public License
C  along with this program; if not, write to the Free Software
C  Foundation, Inc., 51 Franklin Street, Fifth Floor,
C  Boston, MA  02110-1301  USA
C
C  François Méot <fmeot@bnl.gov>
C  Brookhaven National Laboratory
C  C-AD, Bldg 911
C  Upton, NY, 11973, USA
C  -------
      SUBROUTINE RESL
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
C     ****************************
C     READS DATA FOR SPIN TRACKING
C     ****************************
      INCLUDE "C.CDF.H"     ! COMMON/CDF/ IES,LF,LST,NDAT,NRES,NPLT,NFAI,NMAP,NSPN,NLOG
      INCLUDE 'MXLD.H'
      INCLUDE "C.DON.H"     ! COMMON/DON/ A(MXL,MXD),IQ(MXL),IP(MXL),NB,NOEL

      PARAMETER (MXSTR=20)
      CHARACTER(20) STRA(MXSTR)
      INTEGER DEBSTR, FINSTR
      CHARACTER(130) TXT
      PARAMETER (KSIZ=10)
      CHARACTER(KSIZ) KLE
      LOGICAL STRCON
      LOGICAL ISNUM

      SAVE MSTR
      DATA MSTR / 4 /

      LINE = 1
      READ(NDAT,FMT='(A)',ERR=90) TXT
      TXT = TXT(DEBSTR(TXT):FINSTR(TXT))
      READ(TXT,*,ERR=90) A(NOEL,1)         ! XL
      IF(STRCON(TXT,'!',
     >                  IS)) TXT = TXT(DEBSTR(TXT):IS-1)
      A(NOEL,2) = 1.d0                        ! Numb. of steps
      A(NOEL,3) = 0.d0                        ! LST (1/2 for log to zoubi.res/plt)

      IF(STRCON(TXT,'split',
     >                      IIS)) THEN
        CALL STRGET(TXT,MSTR,
     >                       NSTR,STRA)
        IF(NSTR .GT. MSTR) CALL ENDJOB
     >  ('Pgm resl. Prblm with ''split'' list : expected, following '
     >  //'''split'', is a list with max. numb. of data = ',MSTR-2)
        I = 3
        DO WHILE (I .LE. NSTR)
          IF(ISNUM(STRA(I)))   ! In case unexpectedly a ! is missing at that line...
     >    READ(STRA(I),*,ERR=90,END=90) A(NOEL,I-1)
          I = I + 1
        ENDDO
      ENDIF

      IF(A(NOEL,2) .LE. 0) THEN
        WRITE(NRES,FMT='(/,5X,''Unexpected value:  N = ''
     >  ,I0,''  (should be >0) '',/)') A(NOEL,2)
        GOTO 90
      ENDIF

      IF(A(NOEL,3) .LT. 0 .OR. A(NOEL,3) .GT. 2) THEN
        WRITE(NRES,FMT='(/,5X,''Unexpected value:  IL = ''
     >  ,I0,''  (should have  0 .le. IL .le. 2 ) '',/)') A(NOEL,2)
        GOTO 90
      ENDIF

      RETURN

 90   CONTINUE
      CALL ZGKLEY(
     >            KLE)
      CALL ENDJOB('*** Pgm resl, keyword '//KLE//' : '//
     >'input data error, at line #',LINE)
      RETURN
      END
