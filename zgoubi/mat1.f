C  ZGOUBI, a program for computing the trajectories of charged particles
C  in electric and magnetic fields
C  Copyright (C) 1988-2007  François Méot
C
C  This program is free software; you can redistribute it and/or modify
C  it under the terms of the GNU General Public License as published by
C  the Free Software Foundation; either version 2 of the License, or
C  (at your option) any later version.
C
C  This program is distributed in the hope that it will be useful,
C  but WITHOUT ANY WARRANTY; without even the implied warranty of
C  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C  GNU General Public License for more details.
C
C  You should have received a copy of the GNU General Public License
C  along with this program; if not, write to the Free Software
C  Foundation, Inc., 51 Franklin Street, Fifth Floor,
C  Boston, MA  02110-1301  USA
C
C  François Méot <fmeot@bnl.gov>
C  Brookhaven National Laboratory
C  C-AD, Bldg 911
C  Upton, NY, 11973, USA
C  -------
      SUBROUTINE MAT1(IT1,IMAX,F,
     >                           R,T)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE "MAXCOO.H"
      DIMENSION F(MXJ,*)
      DIMENSION R(6,6) , T(6,6,6)
      
      INCLUDE "MAXTRA.H"
      INCLUDE "C.SPIN.H"     ! COMMON/SPIN/ KSPN,KSO,SI(4,MXT),SF(4,MXT)
      INCLUDE "C.OBJET.H"     ! COMMON/OBJET/ FO(MXJ,MXT),KOBJ,IDMAX,IMAXT,KZOB
      
      DIMENSION G(2,6), GO(2,6)
      SAVE G

C Track ITRM trajectories for matrix computation
      PARAMETER (ITRM = 13)         ! (ITRM = 11)
       
C     -------------------------------------------------
C     OPTION  IORD = 1 :
C       MATRICES ORDRE 1 ET 2, SERIES DE TAYLOR ORDRE 3
C     -------------------------------------------------
C      INCLUDE "C.FAISC.H"     ! COMMON/FAISC/ F(MXJ,MXT),AMQ(5,MXT),DP0(MXT),IMAX,IEX(MXT),
C     $     IREP(MXT),AMQLU,PABSLU

      R(:,:) = 0.D0
      R(5,5) = 1.D0
      R(6,6) = 1.D0
      T(:,:,:) = 0.D0
      S1 = 2.D0 * F(6,IT1)

C..............................................
C             Y     T     Z     P     L     D
C
C        Y   R11   R12   R13   R14   R15   R16
C        T   R21   R22   R23   R24   R25   R26
C        Z   R31   R32   R33   R34   R35   R36
C        P   R41   R42   R43   R44   R45   R46
C        L   R51   R52   R53   R54   R55   R56
C        D   R61   R62   R63   R64   R65   R66
C..............................................

C Paticles with dp/p offset:
      IDP = IT1+9
      IDM = IT1+10
      
      DP = (FO(1,IDP) - FO(1,IDM) ) / (.5D0*( FO(1,IDP) + FO(1,IDM)))
      
C      DP2 = DP*DP
      DO J=2,5
C      DO J=2,6
        DO I=1,4
C        DO I=1,5
          I2 = 2*I +IT1-1
          I3 = I2+1
          UO = FO(I+1,I2)-FO(I+1,I3)
          R(J-1,I)  = (F(J,I2) - F(J,I3)) / UO
          IF(J .EQ. 5) R(5,I)  = ( F(6,I2) - F(6,I3) ) / UO
        ENDDO
        R(J-1,6)  = (F(J,IDP) - F(J,IDM)) /DP
      ENDDO
      R(5,6)  = ( F(6,IDP) - F(6,IDM) ) / DP

C      IF(IMAX.LT.13) RETURN
      IF(ITRM.LT.13) RETURN
      
C     ... Compute Ri5, i=1,5.
      IF(FO(6,12)-FO(6,13) .NE. 0.D0) THEN
        DL=FO(6,12)-FO(6,13)
        R(1,5)  = ( F(2,12) - F(2,13) ) / DL
        R(2,5)  = ( F(3,12) - F(3,13) ) / DL
        R(3,5)  = ( F(4,12) - F(4,13) ) / DL
        R(4,5)  = ( F(5,12) - F(5,13) ) / DL
        R(5,5)  = ( F(6,12) - F(6,13) ) / DL
      
        IF(KSPN.EQ.1) THEN
C Spin transport
          DO J=1,2
            DO I=1,4
              I2 = 2*I +IT1-1
              I3 = I2+1
              UO = FO(I+1,I2)-FO(I+1,I3)
              G(J,I)  = (SF(J,I2) - SF(J,I3)) / UO
            ENDDO
            G(J,6)  = ( SF(J,IDP) - SF(J,IDM) ) / DP
            G(J,5)  = ( SF(J,12) - SF(J,13) ) / DL
          ENDDO       
        ENDIF
       
      ENDIF
      
      RETURN

      ENTRY MAT11(
     >            GO)
      GO(:,:) = G(:,:)
      RETURN
      
      END
