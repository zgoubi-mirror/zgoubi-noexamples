C  ZGOUBI, a program for computing the trajectories of charged particles
C  in electric and magnetic fields
C  Copyright (C) 1988-2007  François Méot
C
C  This program is free software; you can redistribute it and/or modify
C  it under the terms of the GNU General Public License as published by
C  the Free Software Foundation; either version 2 of the License, or
C  (at your option) any later version.
C
C  This program is distributed in the hope that it will be useful,
C  but WITHOUT ANY WARRANTY; without even the implied warranty of
C  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C  GNU General Public License for more details.
C
C  You should have received a copy of the GNU General Public License
C  along with this program; if not, write to the Free Software
C  Foundation, Inc., 51 Franklin Street, Fifth Floor,
C  Boston, MA  02110-1301  USA
C
C  François Méot <fmeot@bnl.gov>
C  Brookhaven National Laboratory
C  C-AD, Bldg 911
C  Upton, NY, 11973, USA
C  -------
      SUBROUTINE OPTICS(
     >                  KOPTCS,LBLOPT,OKCPLD)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      CHARACTER(*) LBLOPT(*)
      LOGICAL OKCPLD
      INCLUDE "C.CDF.H"         ! COMMON/CDF/ IES,LF,LST,NDAT,NRES,NPLT,NFAI,NMAP,NSPN,NLOG
      INCLUDE "C.CONST.H"     ! COMMON/CONST/ CL9,CL ,PI,RAD,DEG,QE ,AMPROT, CM2M
      INCLUDE 'MXLD.H'
      INCLUDE "C.DON.H"     ! COMMON/DON/ A(MXL,MXD),IQ(MXL),IP(MXL),NB,NOEL
C      PARAMETER (MXTA=45) ; PARAMETER (LNTA=132) ; CHARACTER(LNTA) TA
      INCLUDE "C.DONT.H"     ! COMMON/DONT/ TA(MXL,MXTA)
      INCLUDE "MAXCOO.H"
      INCLUDE "MAXTRA.H"
      LOGICAL AMQLU(5),PABSLU
      INCLUDE "C.FAISC.H"     ! COMMON/FAISC/ F(MXJ,MXT),AMQ(5,MXT),DP0(MXT),IMAX,IEX(MXT),
C     $     IREP(MXT),AMQLU,PABSLU
      PARAMETER (LBLSIZ=20)
      CHARACTER(LBLSIZ) LABEL
      INCLUDE "C.LABEL.H"     ! COMMON/LABEL/ LABEL(MXL,2)
      INCLUDE "C.OBJET.H"     ! COMMON/OBJET/ FO(MXJ,MXT),KOBJ,IDMAX,IMAXT,KZOB
      INCLUDE "C.PTICUL.H"     ! COMMON/PTICUL/ AM,Q,G,TO
      INCLUDE "C.REBELO.H"   ! COMMON/REBELO/ NRBLT,IPASS,KWRT,NNDES,STDVM
      INCLUDE "C.RIGID.H"     ! COMMON/RIGID/ BORO,DPREF,HDPRF,DP,QBR,BRI
      INCLUDE "C.SYNRA.H"     ! COMMON/SYNRA/ KSYN


      LOGICAL OKLNO, FITING
      SAVE OKLNO
      LOGICAL IDLUNI
      CHARACTER(20) FRMT
      
      DATA OKLNO / .FALSE. /
      DATA  LNOPT / 999 /
      
      IF(KSYN.EQ.1)
     >CALL ENDJOB('Pgm optics. OPTICS is not compatible with SRLOSS.'
     >//' Swith SR off first. ',-99)

      XOPT = A(NOEL,1)
      KOPTCS = INT(XOPT)
      IF(KOPTCS .GT. 1)
     >CALL ENDJOB('Sbr optics. No such option IOPT = ',KOPTCS)

      CALL FITSTA(5,
     >              FITING)
      IF(FITING .AND. KOPTCS .NE. 0) CALL ENDJOB('Pgm optics. OPTICS'
     >//' is not compatible with FIT. Inhibit OPTICS, first',-99)      
      
      NLBL = NINT(A(NOEL,2))
      DO I = 1, NLBL
        LBLOPT(I) = TA(NOEL,I+2)  
      ENDDO

      IF(TA(NOEL,1) .EQ. 'PRINT') THEN
        KIMP = 1
      ELSE
        KIMP = 0
      ENDIF
      OKCPLD = TA(NOEL,2) .EQ. 'coupled'
      CALL MATRI2(OKCPLD)
      
      IF(NRES.GT.0) THEN
        WRITE(NRES,*) ' '
        WRITE(NRES,FMT='(10X,A,I0,A,I0,/)')
     >  'KOPTCS = ',KOPTCS,'   (off/on = 0/1) ;  '
     >  //' number of label classes to account for:  NLBL = ',NLBL
        WRITE(FRMT,FMT='(I0)') NLBL
        FRMT = '(10X,A,'//TRIM(FRMT)//'A,/)'
C        WRITE(NRES,FMT='(10X,20A)')
        WRITE(NRES,FMT=FRMT)
     >  'Beam matrix print out at following labels:  ',
     >       (TRIM(LBLOPT(I))//' ',I=1, NLBL)
        WRITE(NRES,FMT='(10X,A,I0,/)') 
     >  'PRINT out of optical functions to zgoubi.OPTICS.out '
     >  //'(no/yes = 0/1), now: ',KIMP
C Sam, Dec 2016
C        WRITE(NRES,FMT='(10X,A,I0)')
        WRITE(NRES,FMT='(10X,A,L1,/)') 'Computation of transport '//
     >  'matrix in coupled optics hypothesis (True/False): ',OKCPLD
      ENDIF

      IF(KIMP.EQ.1) THEN
        IF(.NOT. OKLNO) THEN
          IF(IDLUNI(
     >              LNOPT)) THEN
            OPEN(UNIT=LNOPT,FILE='zgoubi.OPTICS.out',ERR=899)
            OKLNO = .TRUE.
          ENDIF
          IF(OKLNO) THEN
            WRITE(LNOPT,FMT='(A)') '# From OPTICS keyword / '
            WRITE(LNOPT,FMT='(A,A1,A8,A5,A)')
     >         '# alfx          btx           alfy          bty  ' //
     >         '         alfl          btl           Dx          ' //
     >         '  Dxp           Dy            Dyp           phix/' //
     >         '2pi      phiy/2pi    cumul_s/m       #lmnt  x/m  ' //
     >         '         xp/rad        y/m           yp/rad      ' //
     >         'KEYWORD    label1     label2       FO(6,1)/m     ' //
     >         'K0*L          K1*L          K2*L          |C|    ' //
     >         '       r       !    optimp.f    ' //
     >         'IPASS           frac(P/Pref) int(%)   R11-R56  '//
     >         '   pathL=F(1,6)   (SN0(JJ),JJ=1,4)  AL  DEV'
            WRITE(LNOPT,FMT='(A,3A2,A)')
     >         '# 1             2             3             4    ' //
     >         '         5             6             7           ' //
     >         '  8             9             10            11   ' //
     >         '         12            13            14     15   ' //
     >         '         16            17            18          ' //
     >         '19         20         21           22            ' //
     >         '23            24            25            26     ' //
     >         '       27      28      29        ' //
     >         '30             31           32       33-45       '//
     >         '46             47-50             51  52 '
          ENDIF
        ENDIF
      ELSE
        OKLNO = .FALSE.
      ENDIF
      CALL OPTIC2(OKLNO,LNOPT)

      RETURN

 899  CONTINUE

      CALL ENDJOB('Pgm optics. KEYWORD OPTICS. Error open '
     >//'zgoubi.OPTICS.out.',-99)
C Debug E. Herbert, BNL, 06/2020
C     >//'zgoubi.OPTICS.out.')
      RETURN
      END
