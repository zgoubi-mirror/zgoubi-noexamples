C  ZGOUBI, a program for computing the trajectories of charged particles
C  in electric and magnetic fields
C  Copyright (C) 1988-2007  François Méot
C
C  This program is free software; you can redistribute it and/or modify
C  it under the terms of the GNU General Public License as published by
C  the Free Software Foundation; either version 2 of the License, or
C  (at your option) any later version.
C
C  This program is distributed in the hope that it will be useful,
C  but WITHOUT ANY WARRANTY; without even the implied warranty of
C  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C  GNU General Public License for more details.
C
C  You should have received a copy of the GNU General Public License
C  along with this program; if not, write to the Free Software
C  Foundation, Inc., 51 Franklin Street, Fifth Floor,
C  Boston, MA  02110-1301  USA
C
C  François Méot <fmeot@bnl.gov>
C  Brookhaven National Laboratory
C  C-AD, Bldg 911
C  Upton, NY, 11973, USA
C  -------
      SUBROUTINE FITSET(SAVFT,FNAME)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL SAVFT
      CHARACTER(*) FNAME
      INCLUDE "C.CDF.H"     ! COMMON/CDF/ IES,LF,LST,NDAT,NRES,NPLT,NFAI,NMAP,NSPN,NLOG
      INCLUDE 'MXLD.H'
      INCLUDE "C.DON.H"     ! COMMON/DON/ A(MXL,MXD),IQ(MXL),IP(MXL),NB,NOEL

      PARAMETER (MXV=60)
      INCLUDE "C.VARY.H"  ! COMMON/VARY/ NV,IR(MXV),NC,I1(MXV),I2(MXV),V(MXV),IS(MXV),W(MXV),
                          !     >IC(MXV),IC2(MXV),I3(MXV),KCOU(MXV,3),CPAR(MXV,27)
      LOGICAL RBLFIT, RBLFII
      SAVE RBLFIT
      LOGICAL NOSYS,NOSYSI
      SAVE NOSYS

      DATA RBLFIT / .FALSE. /
      DATA NOSYS / .FALSE. /

C Set to T by REBELOTE, always. That allows it possibly embedded within FIT
      IF(RBLFIT) THEN
        IPASS = 1      ! Otherwise, it has value IPASS+1 as set at end of REBELOTE
        CALL REBEL8(RBLFIT)
      ENDIF

C-----  structure length ------------
      CALL SCUMS(ZERO)
C------------------------------------------------

      IF(NRES.GT.0) NRES=-NRES
      NB = NOEL-1

      IF(NOSYS) THEN
        CALL IMPVA2(.TRUE.,.FALSE.)
        CALL IMPCT2(.TRUE.,.FALSE.)
      ELSE
        IF(NRES.GT.0) WRITE(NRES,FMT=
     >  '(/,20X,''Use "noSYSout" to inhibit sys$output during FIT'')')
      ENDIF

      IF(SAVFT) THEN
        IF(NRES.GT.0) WRITE(NRES,FMT=
     >  '(/,20X,''Final FIT status will be saved in '',A,/)')
     >  FNAME
      ELSE
        IF(NRES.GT.0) WRITE(NRES,FMT=
     >  '(/,20X,''Final FIT status will NOT be saved. For so, use the'',
     >   '' ''''save [FileName]'''' command'')')
      ENDIF

      IF(NRES.GT.0) WRITE(NRES,FMT=
     >'(/,20X,''Use "nofinal" to avoid one last run after FIT'')')

      CALL FITMM8(CPAR)

      NI= 0 ; MXIRT = 0
      CALL FITSTE(NI,MXIRT)
      
      RETURN

      ENTRY FITSE2(RBLFII)
      RBLFIT = RBLFII
      RETURN

      ENTRY FITSE4(NOSYSI)
      NOSYS=NOSYSI
      RETURN
      
      END
